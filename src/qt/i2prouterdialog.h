#ifndef I2PROUTERDIALOG_H
#define I2PROUTERDIALOG_H

#include <memory>
#include <QDialog>

namespace Ui {
class I2PRouterDialog;
}

class I2PRouterDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit I2PRouterDialog(QWidget *parent = 0);
    ~I2PRouterDialog();
    
private:
    std::unique_ptr<Ui::I2PRouterDialog> ui;

private slots:
    void RunPeerTest();
	void TransitTunnels (int enabled);
};

#endif 
