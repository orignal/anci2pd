#include <QStringList>
#include <QStringListModel>
#include "i2prouterdialog.h"
#include "ui_i2prouterdialog.h"
#include "api.h" // i2pd
#include "i2p.h"

I2PRouterDialog::I2PRouterDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::I2PRouterDialog)
{
    ui->setupUi(this);
	switch (i2p::context.GetStatus ())
	{
		case i2p::eRouterStatusOK:
			ui->networkStatus->setText ("OK");
		break;
		case i2p::eRouterStatusTesting:
			ui->networkStatus->setText ("testing");
		break;
		case i2p::eRouterStatusFirewalled:
			ui->networkStatus->setText ("firewalled");
		break;
		default:
			ui->networkStatus->setText ("unknown");
	}	
	ui->transitTunnelsCheckBox->setChecked (i2p::context.AcceptsTunnels ());

	QStringList streamsList;
	auto dest = I2PSession::Instance ().GetLocalDestination ();
	if (dest)
	{
		ui->localAddress->setText (QString::fromStdString(dest->GetIdentHash ().ToBase32 () + ".b32.i2p"));	
		auto sdest = dest->GetStreamingDestination ();
		if (sdest)
		{
			for (auto stream: sdest->GetStreams ())
			{
				auto ident = stream.second->GetRemoteIdentity ();
				if (ident)
					streamsList <<QString::fromStdString (ident->GetIdentHash ().ToBase32 () + ".b32.i2p");
				else
					streamsList << "unknown";
			}
		}
	}
	ui->streamsListView->setModel (new QStringListModel (streamsList));

	QObject::connect(ui->runPeerTestButton, SIGNAL(clicked()), this, SLOT(RunPeerTest()));
	QObject::connect(ui->transitTunnelsCheckBox, SIGNAL(stateChanged(int)), this, SLOT(TransitTunnels (int)));
}

I2PRouterDialog::~I2PRouterDialog()
{
}

void I2PRouterDialog::RunPeerTest()
{
	i2p::api::RunPeerTest ();
}

void I2PRouterDialog::TransitTunnels (int enabled)
{
	i2p::context.SetAcceptsTunnels (enabled);
}
